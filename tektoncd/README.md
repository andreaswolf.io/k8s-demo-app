# tekton example
https://github.com/kameshsampath/pipeline-helloworld
# tekton docs
https://github.com/tektoncd/pipeline

# install tekton
https://github.com/tektoncd/pipeline/blob/master/docs/install.md

kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml

# TODO Persistent Volume for sharing resources



# Running
Create Service Account called pipeline and make it as cluster-admin

kubectl create sa pipeline
kubectl create clusterrolebinding pipeline-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=demo-app:pipeline

kubectl create -f build-resources.yaml

kubectl create -f app-build-task.yaml
kubectl create -f app-kube-deploy.yaml

kubectl create -f app-build-task-run.yaml

kubectl create -f app-deploy.yaml

tkn pipeline start   --resource="app-source=git-source" --resource="app-image=getting-started-image" --serviceaccount='pipeline' app-deploy