# run local registry
docker run -d -p 5000:5000 --restart=always --name registry registry:2

# create namespace
kubectl create -f cluster-namespace.yaml

# create local context
kubectl config set-context demo-app --cluster=docker-desktop --namespace=demo-app --user=docker-desktop
kubectl config use-context demo-app

# create deployment -  frontend

kubectl apply --namespace=demo-app -f frontend-deployment.yaml

# create service -  frontend

kubectl apply -f frontend-service.yaml



# helpful commands

## start helper container in k8s
kubectl run -it --rm busybox --image=busybox --restart=Never -- sh

## curl something in k8s
kubectl run -it --rm curl --image=curlimages/curl --restart=Never -- curl registry.demo-app