
# Manipulate hosts, to fix docker DNS problems
https://github.com/docker/for-mac/issues/3611#issuecomment-479507361

/etc/hosts

127.0.0.1	localhost registry.me
::1         localhost registry.me

### port-forwarding for external access
kubectl port-forward --address localhost svc/registry 5000:80

