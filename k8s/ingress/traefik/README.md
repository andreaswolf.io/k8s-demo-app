# Ingress-Controller for local development
kubectl apply -f crd/crd.yaml
kubectl apply -f traefik-ingress.yaml
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml

# for external access port forwarding to service traefik
kubectl port-forward --address 0.0.0.0 service/traefik 80:80
#kubectl port-forward --address 0.0.0.0 service/traefik 80:80 443:443

# view logs
kubectl logs -f traefik-XXX