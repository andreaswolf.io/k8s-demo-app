# Build docker image

`docker image build -t frontend-demo .`

# Run image

`docker container run --name frotend-demo --rm -d -p 80:80 frontend-demo`

#push image
docker tag frontend-demo localhost:5000/frontend-demo
docker push localhost:5000/frontend-demo
