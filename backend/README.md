cd quarkus-quickstarts/getting-started

mvn package
docker build -f src/main/docker/Dockerfile.jvm -t quarkus/getting-started-jvm .

docker run -i --rm -p 8080:8080 quarkus/getting-started-jvm

curl localhost:8080/hello/greeting/andreas

# docker push 
docker tag quarkus/getting-started-jvm registry.me:5000/quarkus/getting-started-jvm
docker push registry.me:5000/quarkus/getting-started-jvm